var express         = require("express"),
    app             = express(),
    bodyParser      = require("body-parser"),
    methodOverride  = require("method-override"),
    cors            = require('cors');



app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride("_method"));
app.use(express.static(__dirname + '/public'));
app.use(cors());

/* Add routers */
var clientes      = require('./routes/clientes'),
    articulos     = require('./routes/articulos'),
    configuracion = require('./routes/configuracion'),
    ventas        = require('./routes/ventas');

app.use('/clientes',clientes);
app.use('/articulos',articulos);
app.use('/configuracion',configuracion);
app.use('/ventas',ventas);

// Start server
app.listen(8000,function() {
  console.log("Running");
});
