var client     = require('./conection')
var shared     = require('./shared');

var conn = client.createConnection()
var sql ="var para consultas SQL";
var jResponse = {};


//GET => get
  exports.get = function(req,res){
    try {
      sql="SELECT ventas.id, clientes.id as id_cliente, CONCAT(clientes.nombre,' ',clientes.first_lastname,' ',clientes.second_lastname) as nombre, ventas.total, ventas.estatus, ventas.fecha  FROM `ventas` LEFT JOIN clientes on ventas.id_cliente = clientes.id";
      conn.query(sql,(err,results) =>{
        if(err){ jResponse = { status:500,message:"Error Server Side BD;" };}
          else { jResponse = { status:200,message:"OK", results:results }; }
        res.status(200).json(jResponse);
      });
    } catch (e) {
      jResponse = {status:500, message:e.message}
      res.status(200).json(jResponse);
    }
  }
//POST  => Create new
  exports.create = function(req,res){
    try {
      shared.functionValidate([req.body.id_cliente, req.body.total, req.body.plazo ],(validateResult)=>{
        if(validateResult){
          sql="INSERT INTO `ventas`(`id`, `id_cliente`, `total`, `fecha`, `estatus`, `plazo`) VALUES (NULL,"+ req.body.id_cliente+","+ req.body.total+",NULL,1,"+ req.body.plazo+")";
          conn.query(sql,(err,results)=>{
            insertDetail(results.insertId,req.body.lista)
            if(err){ jResponse = { status:500, message:"Server side Error BD" }; }
              else { jResponse = { status:200, message:"OK" } }
            res.status(200).json(jResponse);
          })
        }else{
          jResponse = {status:405, message:"Missin Parameters"}
          res.status(200).json(jResponse);
        }
      })
    } catch (e) {
      jResponse = {status:500, message:e.message}
      res.status(200).json(jResponse);
    }
  }

//No Http Action => inserta los articulos y cantidad compradas en la tabla de detalle de ventas

function insertDetail(id_venta,items_Buy) {
  for (var i = 0; i < items_Buy.length; i++) {
    let query = "INSERT INTO `detalle_venta`(`id`, `id_venta`, `id_articulo`, `cantidad`) VALUES (NULL,"+ id_venta +","+ items_Buy[i].id +" ,"+ items_Buy[i].cantidad+")";
    updateInventario(items_Buy[i].inventario,items_Buy[i].cantidad,items_Buy[i].id);
    conn.query(query,(err)=>{
      if (err) throw err;
    })
  }
}

function updateInventario(existencia,cantidad,id){
  let query2 ="UPDATE `articulos` SET `existence`=( existence -"+ cantidad +") WHERE id="+id;
  sql
  conn.query(query2,(error)=>{
    if (error) throw error;
  })
}

//GET => Find  by id
  exports.findyById = function(req,res){
    try {
      sql = "SELECT * from `ventas` WHERE id = " +req.params.id;
      conn.query(sql,(err,results)=>{
        if(err){ jResponse = { status:500, message:"Server side Error BD" }; }
          else { jResponse = { status:200, message:"OK", results:results[0]} }
        res.status(200).json(jResponse);
      });
    } catch (e) {
      jResponse = {status:500, message:e.message}
      res.status(200).json(jResponse);
    }
  }

  //Get => Obtiene el siguiente id a insertar
  exports.getNewId = function (req,res) {
    try {
      sql ="SHOW TABLE STATUS FROM `la_vendimia` WHERE `name` LIKE 'ventas'";
      conn.query(sql,(err,results) => {
        if(err){ throw err; jResponse = { status:500, message:"Server side Error BD" }; }
          else { jResponse = { status:200, message:"OK", results:results[0] } }
        res.status(200).json(jResponse);
      })
    } catch (e) {
      jResponse = {status:500, message:e.message}
      res.status(200).json(jResponse);
    }
  }
