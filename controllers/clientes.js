var client     = require('./conection')
var shared     = require('./shared');

var conn = client.createConnection()
var sql ="var para consultas SQL";
var jResponse = {};


//GET => get
  exports.get = function(req,res){
    try {
      sql="SELECT * FROM `clientes`";
      conn.query(sql,(err,results) =>{
        if(err){ jResponse = { status:500,message:"Error Server Side BD;" };}
          else { jResponse = { status:200,message:"OK", results:results }; }
        res.status(200).json(jResponse);
      });
    } catch (e) {
      jResponse = {status:500, message:e.message}
      res.status(200).json(jResponse);
    }
  }
//POST  => Create new
  exports.create = function(req,res){
    try {
      shared.functionValidate([req.body.name, req.body.first_lastname, req.body.second_lastname, req.body.rfc ],(validateResult)=>{
        if(validateResult){
          sql="INSERT INTO `clientes`(`id`, `nombre`, `first_lastname`, `second_lastname`, `rfc`) VALUES (NULL,'"+ req.body.name +"','"+ req.body.first_lastname +"','"+ req.body.second_lastname +"','"+ req.body.rfc +"')";
          conn.query(sql,(err)=>{
            if(err){ jResponse = { status:500, message:"Server side Error BD" }; }
              else { jResponse = { status:200, message:"OK" } }
            res.status(200).json(jResponse);
          })
        }else{
          jResponse = {status:405, message:"Missin Parameters"}
          res.status(200).json(jResponse);
        }
      })
    } catch (e) {
      jResponse = {status:500, message:e.message}
      res.status(200).json(jResponse);
    }
  }
//GET => Find  by id
  exports.findyById = function(req,res){
    try {
      sql = "SELECT * from `clientes` WHERE id = " +req.params.id;
      conn.query(sql,(err,results)=>{
        if(err){ jResponse = { status:500, message:"Server side Error BD" }; }
          else { jResponse = { status:200, message:"OK", results:results[0]} }
        res.status(200).json(jResponse);
      });
    } catch (e) {
      jResponse = {status:500, message:e.message}
      res.status(200).json(jResponse);
    }
  }
//PUT => Update
  exports.update = function(req,res){
    try {
      shared.functionValidate([req.body.name, req.body.first_lastname, req.body.second_lastname, req.body.rfc  ],(validateResult)=>{
        if(validateResult){
          sql="UPDATE `clientes` SET `nombre`='"+ req.body.name +"',`first_lastname`='"+ req.body.first_lastname +"',`second_lastname`='"+ req.body.second_lastname +"',`rfc`='"+ req.body.rfc +"' WHERE `id`=" +req.params.id;
          conn.query(sql,(err)=>{
            if(err){ jResponse = { status:500, message:"Server side Error BD" }; }
              else { jResponse = { status:200, message:"OK" } }
            res.status(200).json(jResponse);
          })
        }else{
          jResponse = {status:405, message:"Missin Parameters"}
          res.status(200).json(jResponse);
        }
      });
    } catch (e) {
      jResponse = {status:500, message:e.message}
      res.status(200).json(jResponse);
    }
  }

  exports.search = function (req,res) {
      try {
        sql ="SELECT CONCAT(nombre, ' ', first_lastname, ' ', second_lastname) as value, id, rfc FROM `clientes` ";
        conn.query(sql,(err,results)=>{
          if(err){ throw err; jResponse = { status:500, message:"Server side Error BD" }; }
            else { jResponse = { status:200, message:"OK", results:results } }
          res.status(200).json(jResponse);
        })
      } catch (e) {
        jResponse = {status:500, message:e.message}
        res.status(200).json(jResponse);
      }
  }

  exports.getNewId = function (req,res) {
    try {
      sql ="SHOW TABLE STATUS FROM `la_vendimia` WHERE `name` LIKE 'clientes'";
      conn.query(sql,(err,results) => {
        if(err){ throw err; jResponse = { status:500, message:"Server side Error BD" }; }
          else { jResponse = { status:200, message:"OK", results:results[0] } }
        res.status(200).json(jResponse);
      })
    } catch (e) {
      jResponse = {status:500, message:e.message}
      res.status(200).json(jResponse);
    }
  }
