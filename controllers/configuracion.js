var client     = require('./conection')
var shared     = require('./shared');

var conn = client.createConnection()
var sql ="var para consultas SQL";
var jResponse = {};


//GET => get
  exports.get = function(req,res){
    try {
      sql="SELECT * FROM `configuracion`";
      conn.query(sql,(err,results) =>{
        if(err){ jResponse = { status:500,message:"Error Server Side BD;" };}
          else { jResponse = { status:200,message:"OK", results:results[0] }; }
        res.status(200).json(jResponse);
      });
    } catch (e) {
      jResponse = {status:500, message:e.message}
      res.status(200).json(jResponse);
    }
  }
//POST  => Create new
  exports.create = function(req,res){
    try {
      shared.functionValidate([req.body.financiamiento, req.body.enganche, req.body.plazo ],(validateResult)=>{
        if(validateResult){
          sql="INSERT INTO `configuracion`(`financiamiento`, `enganche`, `plazo`) VALUES ("+ req.body.financiamiento +","+ req.body.enganche +","+ req.body.plazo +")";
          conn.query(sql,(err)=>{
            if(err){ jResponse = { status:500, message:"Server side Error BD" }; }
              else { jResponse = { status:200, message:"OK" } }
            res.status(200).json(jResponse);
          })
        }else{
          jResponse = {status:405, message:"Missin Parameters"}
          res.status(200).json(jResponse);
        }
      })
    } catch (e) {
      jResponse = {status:500, message:e.message}
      res.status(200).json(jResponse);
    }
  }
//PUT => Update
  exports.update = function(req,res){
    try {
      shared.functionValidate([ req.body.financiamiento, req.body.enganche, req.body.plazo ],(validateResult)=>{
        if(validateResult){
          sql="UPDATE `configuracion` SET `financiamiento`="+ req.body.financiamiento +",`enganche`="+ req.body.enganche +",`plazo`=" + req.body.plazo;
          conn.query(sql,(err)=>{
            if(err){ jResponse = { status:500, message:"Server side Error BD" }; }
              else { jResponse = { status:200, message:"OK" } }
            res.status(200).json(jResponse);
          })
        }else{
          jResponse = {status:405, message:"Missin Parameters"}
          res.status(200).json(jResponse);
        }
      });
    } catch (e) {
      jResponse = {status:500, message:e.message}
      res.status(200).json(jResponse);
    }
  }

/* END Seed */
