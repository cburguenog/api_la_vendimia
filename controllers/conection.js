var mysql = require('mysql');

//mysql client
exports.createConnection = function(){
    var client = mysql.createPool({
      connectionLimit : 40,
      host            : '127.0.0.1',
      user            : 'root',
      password        : '',
      database        : 'la_vendimia',
      port            : '3306'
    });
  return client;
}
