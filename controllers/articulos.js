var client     = require('./conection')
var shared     = require('./shared');

var conn = client.createConnection()
var sql ="var para consultas SQL";
var jResponse = {};


//GET => get
  exports.get = function(req,res){
    try {
      sql="SELECT * FROM `articulos`";
      conn.query(sql,(err,results) =>{
        if(err){ jResponse = { status:500,message:"Error Server Side BD;" };}
          else { jResponse = { status:200,message:"OK", results:results }; }
        res.status(200).json(jResponse);
      });
    } catch (e) {
      jResponse = {status:500, message:e.message}
      res.status(200).json(jResponse);
    }
  }
//POST  => Create new
  exports.create = function(req,res){
    try {
      shared.functionValidate([req.body.description, req.body.model, req.body.price, req.body.existence ],(validateResult)=>{
        if(validateResult){
          sql="INSERT INTO `articulos`(`id`, `description`, `model`, `price`, `existence`) VALUES (NULL,'"+ req.body.description +"','"+ req.body.model +"',"+ req.body.price +",'"+ req.body.existence +"')";
          conn.query(sql,(err)=>{
            if(err){ jResponse = { status:500, message:"Server side Error BD" }; }
              else { jResponse = { status:200, message:"OK" } }
            res.status(200).json(jResponse);
          })
        }else{
          jResponse = {status:405, message:"Missin Parameters"}
          res.status(200).json(jResponse);
        }
      })
    } catch (e) {
      jResponse = {status:500, message:e.message}
      res.status(200).json(jResponse);
    }
  }
//GET => Find  by id
  exports.findyById = function(req,res){
    try {
      sql = "SELECT * from `articulos` WHERE id = " +req.params.id;
      conn.query(sql,(err,results)=>{
        if(err){ jResponse = { status:500, message:"Server side Error BD" }; }
          else { jResponse = { status:200, message:"OK", results:results[0]} }
        res.status(200).json(jResponse);
      });
    } catch (e) {
      jResponse = {status:500, message:e.message}
      res.status(200).json(jResponse);
    }
  }
//PUT => Update
  exports.update = function(req,res){
    try {
      shared.functionValidate([ req.body.description, req.body.model, req.body.price, req.body.existence ],(validateResult)=>{
        if(validateResult){
          sql="UPDATE `articulos` SET `description`='"+ req.body.description +"',`model`='"+ req.body.model +"',`price`="+ req.body.price +",`existence`="+ req.body.existence +" WHERE `id`=" +req.params.id;
          conn.query(sql,(err)=>{
            if(err){ jResponse = { status:500, message:"Server side Error BD" }; }
              else { jResponse = { status:200, message:"OK" } }
            res.status(200).json(jResponse);
          })
        }else{
          jResponse = {status:405, message:"Missin Parameters"}
          res.status(200).json(jResponse);
        }
      });
    } catch (e) {
      jResponse = {status:500, message:e.message}
      res.status(200).json(jResponse);
    }
  }

  //GET => busca items para hacer el autocomplete
  exports.search = function (req,res) {
      try {
        sql ="SELECT `id`, `description` as value, `model`, `price`, `existence` FROM `articulos`";
        conn.query(sql,(err,results)=>{
          if(err){ throw err; jResponse = { status:500, message:"Server side Error BD" }; }
            else { jResponse = { status:200, message:"OK", results:results } }
          res.status(200).json(jResponse);
        })
      } catch (e) {
        jResponse = {status:500, message:e.message}
        res.status(200).json(jResponse);
      }
  }

  //Get => Obtiene el siguiente id a insertar

  exports.getNewId = function (req,res) {
    try {
      sql ="SHOW TABLE STATUS FROM `la_vendimia` WHERE `name` LIKE 'articulos'";
      conn.query(sql,(err,results) => {
        if(err){ throw err; jResponse = { status:500, message:"Server side Error BD" }; }
          else { jResponse = { status:200, message:"OK", results:results[0] } }
        res.status(200).json(jResponse);
      })
    } catch (e) {
      jResponse = {status:500, message:e.message}
      res.status(200).json(jResponse);
    }
  }
