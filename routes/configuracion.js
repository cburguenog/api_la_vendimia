//express
var express = require('express');
//Controller-bd-action
var bd = require('../controllers/configuracion');

var Configuracion = express.Router();

Configuracion.route("/")
    .get(bd.get);
Configuracion.route("/new_configuration")
    .post(bd.create);
Configuracion.route('/update')
    .put(bd.update)

module.exports = Configuracion;
