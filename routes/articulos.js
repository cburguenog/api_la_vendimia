//express
var express = require('express');
//Controller-bd-action
var bd = require('../controllers/articulos');

var Articulos = express.Router();

Articulos.route("/")
    .get(bd.get);
Articulos.route("/new_item")
    .post(bd.create);
Articulos.route('/buscar/:param')
    .get(bd.search)
Articulos.route('/obtener_clave')
    .get(bd.getNewId)
Articulos.route('/:id')
    .get(bd.findyById)
Articulos.route('/:id/update')
    .put(bd.update)

module.exports = Articulos;
