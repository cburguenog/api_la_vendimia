//express
var express = require('express');
//Controller-bd-action
var bd = require('../controllers/ventas');

var Ventas = express.Router();

Ventas.route("/")
    .get(bd.get);
Ventas.route("/nueva_venta")
    .post(bd.create);
Ventas.route("/obtener_clave")
    .get(bd.getNewId)
Ventas.route('/:id')
    .put(bd.findyById)

module.exports = Ventas;
