//express
var express = require('express');
//Controller-bd-action
var bd = require('../controllers/clientes');

var Clientes = express.Router();

Clientes.route("/")
    .get(bd.get);
Clientes.route("/new_client")
    .post(bd.create);
Clientes.route('/buscar/:param')
    .get(bd.search)
Clientes.route('/obtener_clave')
    .get(bd.getNewId)
Clientes.route('/:id')
    .get(bd.findyById)
Clientes.route('/:id/update')
    .put(bd.update)

module.exports = Clientes;
